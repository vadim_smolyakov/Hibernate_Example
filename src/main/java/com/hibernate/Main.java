package com.hibernate;

import com.hibernate.models.Car;
import com.hibernate.models.User;
import com.hibernate.services.UserService;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException {

        UserService userService = new UserService();
        User user = new User("Masha",26);
        userService.saveUser(user);
        Car ferrari = new Car("Ferrari", "red");
        ferrari.setUser(user);
        user.addCar(ferrari);
        Car ford = new Car("Ford", "black");
        ford.setUser(user);
        user.addCar(ford);
        userService.updateUser(user);
    }

}
